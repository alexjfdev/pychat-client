# PyChat Client
##### Pychat is a small piece of software written in python to allow for anonymous communication between people over the internet within the Python shell.

This is the client version. If you want to host your own server for others to connect to please download the PyChat server.
